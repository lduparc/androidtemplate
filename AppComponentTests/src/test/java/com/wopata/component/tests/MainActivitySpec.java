package com.wopata.component.tests;

import com.wopata.R;
import com.wopata.main.MainActivity_;
import com.wopata.component.tests.pages.StartPage;
import com.wopata.test.support.ComponentTestSpecification;

import org.junit.Before;
import org.junit.Test;

public class MainActivitySpec extends ComponentTestSpecification<MainActivity_> {

    StartPage startPage = new StartPage(this);

    public MainActivitySpec() {
        super(MainActivity_.class);
    }

    @Before
    public void setUp() {
        startActivity();
    }

    @Test
    public void testShouldUseCorrectLayout() throws Exception {
        startPage.checkLayoutIs(R.id.activity_main);
    }

    @Test
    public void testShouldShowText() throws Exception {
        startPage.checkTextViewHasText("Hello Espresso!");
    }

}
