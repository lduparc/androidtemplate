package com.wopata.specs;

import android.test.suitebuilder.annotation.LargeTest;

import com.wopata.main.MainActivity_;
import com.wopata.test.support.EspressoSpec;
import com.wopata.test.support.pages.StartPage;

@LargeTest
public class ExampleAppSpec extends EspressoSpec<MainActivity_> {

    StartPage startPage = new StartPage();

    public ExampleAppSpec() {
        super(MainActivity_.class);
    }

    public void testShouldShowText() {
        startPage.checkTextViewHasText("Hello Espresso!");
    }
}
