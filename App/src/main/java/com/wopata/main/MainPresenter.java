package com.wopata.main;

import com.wopata.databaseexample.DatabaseActivity_;
import com.wopata.restexample.RestActivity_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

@EBean
public class MainPresenter {

    @RootContext
    MainActivity view;

    public void onOpenDatabaseExample() {
        DatabaseActivity_.intent(view).start();
    }

    public void onOpenRestExample() {
        RestActivity_.intent(view).start();
    }
}
