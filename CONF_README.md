## Decide what you need
_(template configuration by deletion)_

#### no database

```
* remove from settings.gradle the GenerateDatabaseClasses module
* remove from App/build.gradle the GenerateDatabaseClasses dependency
* reimport gradle
* remove the GenerateDatabaseClasses module
```

#### no network

```
* remove from settings.gradle the GenerateJsonRestModel module
* remove from App/build.gradle the GenerateJsonRestModel dependency
* reimport gradle
* remove the GenerateJsonRestModel module
* After you removed a part there a many more obsolete artefacts, but you will find them easy.
```

#### Rename your app

```
* AndroidManifest.xml change the name at android:label="TemplateApp"
```
