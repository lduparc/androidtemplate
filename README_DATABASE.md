## Modify the database schema
_How to write database schema specifications for RoboCoP read README.MD at [RoboCoP](https://github.com/mediarain/RoboCoP_)

```
* remove content of AndroidSample/src/gen
* specify your new database schema at GenerateDatabaseContent/schema
* generate new database classes with GenerateDatabaseContent:contentProviderGen
```